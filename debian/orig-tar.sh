#!/bin/sh -e

VERSION=$2
TAR=../activemq-activeio_$VERSION.orig.tar.xz
DIR=activemq-activeio-$VERSION
TAG=$(echo "activeio-parent-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export http://svn.apache.org/repos/asf/activemq/activeio/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG
